This is a simple url shortener app written in Django.

An example:

Turn this URL: https://www.youtube.com/watch?v=BpUp8jn-GuI 
into this short URL: http://127.0.01/shorturl/qk1jh0 

The project aims to provide a Django app for this sort of functionality. This includes:

 * submission of urls
 * listing, plus number of access 
 * redirection to the original url
